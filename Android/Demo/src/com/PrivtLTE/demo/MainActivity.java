package com.PrivtLTE.demo;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.os.Build;

public class MainActivity extends Activity {

    private Camera mCamera;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {
		
		private EditText mIpAddrEdit;
		private Button mCall;
		private Spinner mVideoFormatSpin, mBitrateSpin, mAudioSendSpin, mAudioReceiveSpin;
		
		String mIpAddr;
		int mVideoFormat, mBitrate, mAudioSend, mAudioReceive;

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			
			mVideoFormat = 1;
			mBitrate = 2048000 / 512000 - 1;
			mAudioSend = 2;
			mAudioReceive = 2;
			
			mIpAddrEdit = (EditText) rootView.findViewById(R.id.ip_addr);
			mCall = (Button)rootView.findViewById(R.id.call_button);
			
			mCall.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					
					String ip = mIpAddrEdit.getText().toString().trim();
					if (ip.equals("")) {
						mIpAddr = "127.0.0.1";
					} else {
						mIpAddr = ip;
					}
					
					// TOOD : video call activity
					Intent intent = new Intent(v.getContext(), VideoCallActivity.class);
					intent.putExtra(VideoCallActivity.EXTRA_IP, mIpAddr);
					intent.putExtra(VideoCallActivity.EXTRA_VIDEO_FORMAT, mVideoFormat);
					intent.putExtra(VideoCallActivity.EXTRA_BITRATE, mBitrate);
					intent.putExtra(VideoCallActivity.EXTRA_AUDIO_SEND, mAudioSend);
					intent.putExtra(VideoCallActivity.EXTRA_AUDIO_RECEIVE, mAudioReceive);
					
					startActivity(intent);
				}
			});
			
			mVideoFormatSpin = (Spinner) rootView.findViewById(R.id.video_format);
			mBitrateSpin = (Spinner) rootView.findViewById(R.id.bitrate_setting);
			mAudioSendSpin = (Spinner) rootView.findViewById(R.id.audio_send_format);
			mAudioReceiveSpin = (Spinner) rootView.findViewById(R.id.audio_receive_format);
			
			mVideoFormatSpin.setSelection(mVideoFormat);
			mBitrateSpin.setSelection(mBitrate);
			mAudioSendSpin.setSelection(mAudioSend);
			mAudioReceiveSpin.setSelection(mAudioReceive);
			
			mVideoFormatSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int pos, long id) {
					// TODO Auto-generated method stub
					
					mVideoFormat = pos;
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			mBitrateSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int pos, long id) {
					// TODO Auto-generated method stub
					
					mBitrate = (pos + 1) * 512000;
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			mAudioSendSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int pos, long id) {
					// TODO Auto-generated method stub
					
					mAudioSend = pos;
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			mAudioReceiveSpin.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> arg0, View arg1,
						int pos, long id) {
					// TODO Auto-generated method stub
					
					mAudioReceive = pos;
					
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					// TODO Auto-generated method stub
					
				}
				
			});
			
			

			return rootView;
		}
	}
	
	   @Override
	    protected void onResume() {
		   
	        super.onResume();

	   }

}
