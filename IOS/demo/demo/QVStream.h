//
//  QVStream.h
//  QuickVision
//
//  Created by a on 16/1/6.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QVStream_h
#define QVStream_h

#import <Foundation/Foundation.h>

#import "QVFormat.h"

typedef enum {
    QV_UP_STREAM = 0,
    QV_DOWN_STREAM,
    QV_BOTH_STREAM,
} QV_STREAM_DIRECTION;


@protocol QVDownStreamDelegate <NSObject>
- (void) OnDownStreamAvailable : (char * ) data length :(int) len params : (void *)params;
@end

@protocol QVUpStreamDelegate <NSObject>
- (void) OnUpStreamAvailable : (char * ) data length :(int) len params : (void *)params;
@end

@interface QVStream : NSObject

- (id) init;
- (void) RegisterDownStreamListener : (id<QVDownStreamDelegate>) listener;
- (void) UnregisterDownStreamListener : (id<QVDownStreamDelegate>) listener;
- (void) RegisterUpStreamListener : (id<QVUpStreamDelegate>) listener;
- (void) UnregisterUpStreamListener : (id<QVUpStreamDelegate>) listener;

- (int) GetStreamId;
- (void) SetStreamId:(int)StreamId;

+ (BOOL) LinkStream:(QVStream *)down up:(QVStream *)up direction:(QV_STREAM_DIRECTION)direction;
+ (BOOL) UnlinkStream:(QVStream *)down up:(QVStream *)up direction:(QV_STREAM_DIRECTION)direction;

- (NSMutableArray<QVDownStreamDelegate> *) DownStreamListen;
- (NSMutableArray<QVUpStreamDelegate> *) UpStreamListen;

@end

#endif /* QVStream_h */
