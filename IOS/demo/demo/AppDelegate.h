//
//  AppDelegate.h
//  demo
//
//  Created by a on 16/1/25.
//  Copyright © 2016年 a. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {

@public
int mVideoWidth, mVideoHeight;
int mBitRate;
int mAudioSend;
int mAudioReceive;
NSString * mIpAddr;
}


@property (strong, nonatomic) UIWindow *window;


@end

