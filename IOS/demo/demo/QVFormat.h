//
//  QVFormat.h
//  QuickVision
//
//  Created by a on 16/1/7.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QVFormat_h
#define QVFormat_h

#import <Foundation/Foundation.h>

const char * KEY_MIME = "mime";
const char * KEY_PLNAME = "plname";
const char * KEY_SAMPLE_RATE = "sample-rate";
const char * KEY_CHANNEL_NUMBER = "channel-number";
const char * KEY_PAYLOAD_TYPE = "payload-type";
const char * KEY_WIDHT = "width";
const char * KEY_HEIGHT = "height";
const char * KEY_BIT_RATE = "bitrate";
const char * KEY_FRAME_RATE = "frame-rate";
const char * KEY_PAC_SIZE = "pac-size";
const char * KEY_PROFILE = "profile";
const char * KEY_PROFILE_LEVEL = "profile-level";
const char * KEY_I_FRAME_INTERVAL = "i-frame-interval";


@interface QVFormat : NSObject

- (BOOL) ContainKey:(const char *) name;
- (int) GetInteger:(const char *) name;
- (float) GetFloat:(const char *) name;
- (const char *) GetString:(const char *) name;
- (void) SetInteger:(const char *) name value:(int)value;
- (void) SetFloat:(const char *) name value:(float)value;
- (void) SetString:(const char *) name value:(const char *)value;
+ (QVFormat *) Create;
+ (QVFormat *) CreateAudioFormat:(const char *)plname sample:(int)sample channel:(int)channel;
+ (QVFormat *) CreateVideoFormat:(const char *)mime width:(int)width height:(int)height;

@end

#endif /* QVFormat_h */
