//
//  QVAudioCodec.h
//  QuickVision
//
//  Created by a on 16/1/9.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QVAudioCodec_h
#define QVAudioCodec_h

#import "QVStream.h"

@interface QVAudioCodec : QVStream

-(id)init;
- (BOOL) initializeWithID : (int)InstId;
- (BOOL) uninitialize;
- (int) SetEncoderCodec:(QVFormat *)fmt;
- (int) SetDecoderCodec:(QVFormat *)fmt;
- (BOOL) StartEncoder;
- (BOOL) StopEncoder;
- (BOOL) StartDecoder;
- (BOOL) StopDecoder;
- (void *) Codec;

- (void) SendData:(int32_t)FrameType PaylaodType:(uint8_t)PayloadType ts:(uint32_t) ts payload:(const uint8_t *)payload PayloadSize:(size_t)PayloadSize fragment:(void *) fragment;

- (int) SetFECStatus:(BOOL)enable;

@end

#endif /* QVAudioCodec_h */
