
#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>

#import "QVStream.h"
    
@interface QVVideoCapture : QVStream

- (id) init;
- (void) InitializeWith : (int)captureId ViewCtrl:(id)ctrl;
//- (void)setOrientation;
- (BOOL) StartCapture :(int)width  height : (int) height fps : (int) fps;
- (BOOL) StopCapture;

@end

