
#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>

#import "QVStream.h"
    
@interface QVVideoEncoder : QVStream

- (id) init;
- (void) initWithConfiguration : (int) width height : (int) height framerate : (int) fr bitrate : (int) br;
- (BOOL) start;
- (BOOL) stop;

@end

