//
//  ViewController.h
//  iview
//
//  Created by a on 15/12/19.
//  Copyright © 2015年 a. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KCVideoViewController : UIViewController

@property(nonatomic, strong)UIView * viewPreview;
@property(nonatomic, strong)UIView * viewDec;

//-(void) decodeFile:(NSString*)fileName fileExt:(NSString*)fileExt;
@end

