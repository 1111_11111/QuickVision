//
//  QVAudioDev.h
//  QuickVision
//
//  Created by a on 16/1/6.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QVAudioDev_h
#define QVAudioDev_h

#import "QVStream.h"

const int kPrefSampleRate = 48000;
const int kMaxSampleRate = 48000;
const int kChannelNum = 1;
const int kBPS = 16;
const double kIOBufferDuration = 0.01;

@interface QVAudioDev : QVStream

-(id) init;
- (BOOL) initializeWithFormat : (QVFormat *)fmt;
- (BOOL) uninitialize;
- (BOOL) StartPlayout;
- (BOOL) StopPlayout;
- (BOOL) StartCapture;
- (BOOL) StopCapture;

@end

#endif /* QVAudioDev_h */
