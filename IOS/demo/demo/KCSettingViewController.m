//
//  KCSetingViewController.m
//  demo
//
//  Created by a on 16/1/25.
//  Copyright © 2016年 a. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "KCSettingViewController.h"
#import "AppDelegate.h"

@interface KCSettingViewController() <UITableViewDataSource>{
    
    UINavigationItem * mBack;

    UITableView * mSetting;
    
    NSArray * mVideoSource;
    NSArray * mBitRate;
    NSArray * mAudioSend;
    NSArray * mAudioReceive;
    
    NSArray * mTitle;
    
    NSMutableArray * mData;
    
    int mVideoItem;
    int mBitrateItem;
    int mAudioSendItem;
    int mAudioReceiveItem;
    
    NSIndexPath * mVideoSelected, * mBitrateSelected, * mAudioSendSelected, * mAudioReceiveSelected;
}

@end

@implementation KCSettingViewController

-(void) demoBack {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) viewDidLoad {
    
    NSLog(@"KCSettingViewController loaded");
    
    self.navigationItem.title = @"Setting";
    
    
    CGRect status = [[UIApplication sharedApplication] statusBarFrame];
    CGRect nav = self.navigationController.navigationBar.frame;
    
    mBack = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIButtonTypeSystem target:self action:@selector(demoBack)];
    
    self.navigationItem.leftBarButtonItem = mBack;
    
    //mSetting = [[UITableView alloc] initWithFrame:CGRectMake(0, status.size.height + nav.size.height, self.view.frame.size.width, self.view.frame.size.height - status.size.height - nav.size.height)];
    
    mSetting = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    [mSetting setDataSource:self];
    [mSetting setDelegate:self];
    
    [self.view addSubview:mSetting];
    
    mVideoSource = [[NSArray alloc] initWithObjects:@"    VGA 640x480@30fps", @"    720P 1280x720@30fps", @"    1080P 1920x1080@fps", nil];
    
    mBitRate = [[NSArray alloc] initWithObjects:@"    512K", @"    1M", @"    1.5M", @"    2M", @"    2.5M", @"    3M", @"    3.5M", @"    4M", nil];
    mAudioSend = [[NSArray alloc] initWithObjects:@"    PCMU", @"    PCMA", @"    ILBC", @"    OPUS", @"    ISAC", @"    G722", nil];
    mAudioReceive = [[NSArray alloc] initWithObjects:@"    PCMU", @"    PCMA", @"    ILBC", @"    OPUS", @"    ISAC", @"    G722", nil];
    
    
    mTitle = [[NSArray alloc] initWithObjects:@"Video Source", @"Bitrate", @"Audio Send", @"Audio Receive", nil];
    
    mData = [[NSMutableArray alloc] init];
    [mData addObject:mVideoSource];
    [mData addObject:mBitRate];
    [mData addObject:mAudioSend];
    [mData addObject:mAudioReceive];
    
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    if(delegate->mVideoWidth == 640) {
        mVideoItem = 0;
    } else if(delegate->mVideoWidth == 1280) {
        mVideoItem = 1;
    } else if(delegate->mVideoWidth == 1920) {
        mVideoItem = 2;
    } else
        mVideoItem = 1;
    
    mAudioSendItem = delegate->mAudioSend;
    mAudioReceiveItem = delegate->mAudioReceive;
    mBitrateItem = delegate->mBitRate / 512000 - 1;
    
    /*
    delegate->mVideoWidth = 1280;
    delegate->mVideoHeight = 720;
    delegate->mBitRate = 2000000;
    delegate->mAudioSend = mAudioSendItem;
    delegate->mAudioReceive = mAudioReceiveItem;
    */
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return mData.count;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray * data = mData[section];
    return data.count;
}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    NSArray * array = mData[indexPath.section];
    NSString * text = array[indexPath.row];
    
    cell.textLabel.text = text;
    
    switch (indexPath.section) {
        case 0: // VIDEO SOURCE
            if(indexPath.row == mVideoItem) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                mVideoSelected = indexPath;
            }
            else
                cell.accessoryType = UITableViewCellAccessoryNone;
            
            break;

        case 1: // BITRATE
            if(indexPath.row == mBitrateItem) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                mBitrateSelected = indexPath;
            }

            else
                cell.accessoryType = UITableViewCellAccessoryNone;
            
            break;
            
            
        case 2: // AUDIO SEND
            if(indexPath.row == mAudioSendItem) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                mAudioSendSelected = indexPath;
            }
            else
                cell.accessoryType = UITableViewCellAccessoryNone;
            break;

        case 3: // AUDIO RECEIVE
            if(indexPath.row == mAudioReceiveItem) {
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
                mAudioReceiveSelected = indexPath;
            }
            else
                cell.accessoryType = UITableViewCellAccessoryNone;
            break;
            
        default:
            break;
    }
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    switch (indexPath.section) {
        case 0: // VIDEO SOURCE
            if(indexPath.row != mVideoItem) {
                
                mVideoItem = indexPath.row;
                
                //delegate->mVideoSource = mVideoSource[mVideoItem];
                switch (mVideoItem) {
                    case 0: // VGA
                        delegate->mVideoWidth = 640;
                        delegate->mVideoHeight = 480;
                        break;
                        
                    case 1: // 720P
                        delegate->mVideoWidth = 1280;
                        delegate->mVideoHeight = 720;

                        break;
                        
                    case 2: // 1080P
                        delegate->mVideoWidth = 1920;
                        delegate->mVideoHeight = 1080;

                        break;
                        
                    default:
                        break;
                }
                
                UITableViewCell * old = [tableView cellForRowAtIndexPath:mVideoSelected];
                old.accessoryType = UITableViewCellAccessoryNone;
                
                UITableViewCell * newCell = [tableView cellForRowAtIndexPath:indexPath];
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                mVideoSelected = indexPath;
                
            }
            
            break;
   
            
        case 1: // BITRATE
            if(indexPath.row != mBitrateItem) {
                
                
                mBitrateItem = indexPath.row;
                
                delegate->mBitRate = 512000 * (mBitrateItem + 1);
                
                UITableViewCell * old = [tableView cellForRowAtIndexPath:mBitrateSelected];
                old.accessoryType = UITableViewCellAccessoryNone;
                
                UITableViewCell * newCell = [tableView cellForRowAtIndexPath:indexPath];
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                mBitrateSelected = indexPath;
                
            }
            
            break;

            
        case 2: // AUDIO SEND
            if(indexPath.row != mAudioSendItem) {
                
                mAudioSendItem = indexPath.row;
                
                delegate->mAudioSend = mAudioSendItem;
                
                UITableViewCell * old = [tableView cellForRowAtIndexPath:mAudioSendSelected];
                old.accessoryType = UITableViewCellAccessoryNone;
                
                UITableViewCell * newCell = [tableView cellForRowAtIndexPath:indexPath];
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                mAudioSendSelected = indexPath;
                
            }

            break;
            
        case 3: // AUDIO RECEIVE
            if(indexPath.row != mAudioReceiveItem) {
                
                mAudioReceiveItem = indexPath.row;
                
                delegate->mAudioReceive = mAudioReceiveItem;
                
                UITableViewCell * old = [tableView cellForRowAtIndexPath:mAudioReceiveSelected];
                old.accessoryType = UITableViewCellAccessoryNone;
                
                UITableViewCell * newCell = [tableView cellForRowAtIndexPath:indexPath];
                newCell.accessoryType = UITableViewCellAccessoryCheckmark;
                
                mAudioReceiveSelected = indexPath;
                
            }

            break;
            
        default:
            break;
    }

}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return mTitle[section];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return self.view.frame.size.height / 30;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    return self.view.frame.size.height / 35;
}


@end