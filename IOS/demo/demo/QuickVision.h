//
//  QuickVision.h
//  QuickVision
//
//  Created by a on 16/2/22.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QuickVision_h
#define QuickVision_h

@interface QuickVision : NSObject

+ (QuickVision *) GetInstance;
- (BOOL) Check;
- (void) Init:(NSString *)license;
@end

#endif /* QuickVision_h */
