
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#import <UIKit/UIKit.h>
#import <VideoToolbox/VideoToolbox.h>

#import "QVStream.h"

@protocol QVVideoDecoderDelegate <NSObject>
- (void)Render:(CVPixelBufferRef *) pixel;
@end

@interface QVVideoDecoder : QVStream

- (id) init;
- (void) initializeWith:(int)width height:(int)height bitrate:(int)bitrate;
- (void) initializeWith : (int) width height : (int) height bitrate : (int) br render:(id<QVVideoDecoderDelegate>)render;

- (BOOL) start;
- (void) stop;

- (void) decoderAndDisplay:(char *) data len:(uint32_t)len entire:(uint32_t)entire; 

@end
