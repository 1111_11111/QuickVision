//
//  AppDelegate.m
//  demo
//
//  Created by a on 16/1/25.
//  Copyright © 2016年 a. All rights reserved.
//

#import "AppDelegate.h"

#import "KCCallViewController.h"
#import "KCVideoViewController.h"

@interface AppDelegate () {
    KCCallViewController * mCall;
    KCVideoViewController * mVideo;
    
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    mVideoWidth = 1280;
    mVideoHeight = 720;
    mBitRate = 2048000;
    mAudioReceive = 2;
    mAudioSend = 2;
    mIpAddr = @"127.0.0.1";
    
    _window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    _window.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    //[[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:23/255 green:23/255 blue:23/255 alpha:1]];
    [[UINavigationBar appearance] setBarStyle:UIBarStyleDefault];
    
    ////////////////// TODO : Call Init View Controller
    
    mCall = [[KCCallViewController alloc] init];
    UINavigationController * rootCtrl = [[UINavigationController alloc] initWithRootViewController:mCall];
    _window.rootViewController = rootCtrl;
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
