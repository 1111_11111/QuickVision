//
//  QVAudioChannel.h
//  QuickVision
//
//  Created by a on 16/1/8.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QVAudioChannel_h
#define QVAudioChannel_h

#import "QVAudioCodec.h"

#define kREDPayloadType 127

@interface QVAudioChannel : QVStream

- (id)init;
- (BOOL) initializeWithID : (int)InstId;
- (BOOL) uninitialize;
- (int) SetSendCodec:(QVFormat *)fmt;
- (int) SetReceiveCodec:(QVFormat *)fmt;
- (int32_t) StartSend;
- (int32_t) StopSend;
- (int32_t) StartReceive;
- (int32_t) StopReceive;
- (int32_t) InitialSendSocket:(const char * )ip port : (uint32_t) port;
- (int32_t) InitialReceiveSocket:(uint32_t)port;
- (BOOL) RegisterCodec:(QVAudioCodec *)codec;

- (void) SetNACKStatus:(BOOL)enable;
- (int) SetREDStatus:(BOOL)enable;

@end

#endif /* QVAudioChannel_h */
