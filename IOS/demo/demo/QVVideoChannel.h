//
//  QVVideoChannel.h
//  QuickVision
//
//  Created by a on 16/1/21.
//  Copyright © 2016年 a. All rights reserved.
//

#ifndef QVVideoChannel_h
#define QVVideoChannel_h

#import "QVStream.h"

#define kH264PLName  "H264"
#define kVideoPayloadType 100

@interface QVVideoChannel : QVStream

- (id)init;
- (BOOL) initializeWithID : (int)InstId;
- (BOOL) uninitialize;
- (int) SetSendCodec:(QVFormat *)fmt;
- (int) SetReceiveCodec:(QVFormat *)fmt;
- (int32_t) StartSend;
- (int32_t) StopSend;
- (int32_t) StartReceive;
- (int32_t) StopReceive;
- (int32_t) InitialSendSocket:(const char * )ip port : (uint32_t) port;
- (int32_t) InitialReceiveSocket:(uint32_t)port;

//- (void) SetNACKStatus:(BOOL)enable NumOfPacket:(int)NumOfPacket;
//- (int) SetREDStatus:(BOOL)enable;
- (void) SetProtectionMode:(BOOL)EnNack fec:(BOOL)EnFec red:(int)PayloadTypeRED fec:(int)PayloadTypeFEC;

@end

#endif /* QVVideoChannel_h */
