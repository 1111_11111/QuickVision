//
//  KCCallViewController.m
//  demo
//
//  Created by a on 16/1/25.
//  Copyright © 2016年 a. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppDelegate.h"

#import "KCCallViewController.h"
#import "KCSettingViewController.h"
#import "KCVideoViewController.h"

@interface KCCallViewController() {
    UITextField * mIpAddr;
    UIButton * mCall;
    
    UINavigationItem * mSetting;
    
}

@end

@implementation KCCallViewController

-(void) demoSetting {
    KCSettingViewController * setting = [[KCSettingViewController alloc] init];
    
    [self.navigationController pushViewController:setting animated:YES];
}

-(void) demoCall {
    KCVideoViewController * video = [[KCVideoViewController alloc] init];
    
    AppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
    delegate->mIpAddr = @"127.0.0.1";
    
    if(strncmp([mIpAddr.text UTF8String], "", 1) != 0)
        delegate->mIpAddr = [[NSString alloc] initWithString:mIpAddr.text];
    
    [self.navigationController pushViewController:video animated:YES];
}

-(void) viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"KCCallViewController loaded");
    
    self.navigationItem.title = @"Quick Vision";
    
    mSetting = [[UIBarButtonItem alloc] initWithTitle:@"Setting" style:UIButtonTypeSystem target:self action:@selector(demoSetting)];
    
    self.navigationItem.rightBarButtonItem = mSetting;
    
    mIpAddr = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width / 2 - 150 / 2, self.view.frame.size.height / 4, 150, 30)];
    mIpAddr.borderStyle = UITextBorderStyleRoundedRect;
    mIpAddr.placeholder = @"enter ip address";
    [self.view addSubview:mIpAddr];
    
    mCall = [UIButton buttonWithType:UIButtonTypeSystem];
    mCall.frame = CGRectMake(self.view.frame.size.width / 2 - 80 / 2, self.view.frame.size.height / 4 + 60, 80, 30) ;
    [mCall setTitle:@"Call" forState:UIControlStateNormal];
    [self.view addSubview:mCall];
    [mCall addTarget:self action:@selector(demoCall) forControlEvents:UIControlEventTouchUpInside];
    
}

@end